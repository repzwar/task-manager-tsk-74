package ru.pisarev.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.endpoint.DataEndpoint;
import ru.pisarev.tm.event.ConsoleEvent;
import ru.pisarev.tm.listener.AuthAbstractListener;

@Component
public class DataJsonSaveJaxBListener extends AuthAbstractListener {

    @NotNull
    @Autowired
    private DataEndpoint dataEndpoint;

    @Nullable
    public String name() {
        return "data-save-json-j";
    }

    @Nullable
    public String arg() {
        return null;
    }

    @Nullable
    public String description() {
        return "Save data to JSON by JaxB.";
    }

    @EventListener(condition = "@dataJsonSaveJaxBListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        dataEndpoint.saveDataJsonJaxB(getSession());
    }

}