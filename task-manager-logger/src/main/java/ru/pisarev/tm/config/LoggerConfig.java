package ru.pisarev.tm.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import ru.pisarev.tm.listener.LogMessageListener;

import javax.jms.ConnectionFactory;
import javax.jms.MessageListener;

import static ru.pisarev.tm.constant.ActiveMQConst.URL;

@ComponentScan("ru.pisarev.tm")
public class LoggerConfig {

    @Bean
    public ConnectionFactory factory() {
        @NotNull final ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(URL);
        factory.setTrustAllPackages(true);
        return factory;
    }

    @Bean
    public MessageListener listener() {
        return new LogMessageListener();
    }

}
