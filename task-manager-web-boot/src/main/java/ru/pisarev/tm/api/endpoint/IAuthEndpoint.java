package ru.pisarev.tm.api.endpoint;

import org.springframework.web.bind.annotation.RequestParam;
import ru.pisarev.tm.dto.Result;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface IAuthEndpoint {

    @WebMethod
    Result login(
            @RequestParam("username") String username,
            @RequestParam("password") String password
    );

    @WebMethod
    Result logout();
}
