package ru.pisarev.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.pisarev.tm.model.AuthorizedUser;
import ru.pisarev.tm.service.ProjectService;
import ru.pisarev.tm.service.TaskService;

@Controller
public class TasksController {

    @Autowired
    private TaskService service;

    @Autowired
    private ProjectService projectService;

    @Secured({"ROLE_USER"})
    @GetMapping("/tasks")
    public ModelAndView index(@AuthenticationPrincipal(errorOnInvalidType = true) final AuthorizedUser user) {
        return new ModelAndView("task-list", "tasks", service.findAll(user.getUserId()));
    }

}
