<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<html>
<head>
    <title>Task manager</title>
</head>
<style>
    h1 {
        font-size: 1.6em;
    }

    a {
        color: darkblue;
    }
</style>
<body>

<table width="100%" height="100%" border="1">
    <tr>
        <td height="35" width="200" nowrap="nowrap" align="center">Task manager</td>
        <td align="right">
            <a href="/projects">Projects</a>
            <a href="/tasks">Tasks</a>
            <sec:authorize access="isAuthenticated()">
                <a href="/logout">Logout</a>
            </sec:authorize>
            <sec:authorize access="!isAuthenticated()">
                <a href="/login">Login</a>
            </sec:authorize>
        </td>
    </tr>
    <tr>
        <td colspan="2" valign="top">
